// Place third party dependencies in the lib folder
//
// Configure loading modules from the lib directory,
// except 'app' ones, 
requirejs.config({
    "baseUrl": "js/lib",
    "paths": {
        "app": "../app",
        "jquery": "../lib/jquery.min",
        "reveal": "../lib/reveal",
        "services": "../lib/services"
    }
});

// Load the main app module to start the app
requirejs(["app/main"]);