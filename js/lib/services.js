define(['jquery', 'reveal'], function ($, Reveal) {


    var buildSideNav = function () {

        //create the html string
        var htmlString = "";

        //compile the string
        $.each($("section"), function (i, v) {
            htmlString += "<div class='menu-item'>" + $(v).attr("nav-title") + "</div>";
        });

        //append it to the menu
        $(".nav-menu").append(htmlString);

        setActive();

        $(".menu-item").click(function () {
            Reveal.slide($(this).index());
        });

    };

    var setActive = function () {
        $(".menu-item").removeClass("active");
        $($(".menu-item")[Reveal.getIndices().h]).addClass("active");

        /*        var fragBoolean = false;
                $.each($(section[Reveal.getIndices().h]).contents(), function (i, v) {
                    if ($(v).hasClass("fragment")) {
                        fragBoolean = true;
                    }
                });

                Reveal.configure({
                    controls: fragBoolean
                });*/
    };

    return {
        setActive: setActive,
        buildSideNav: buildSideNav
    };
});