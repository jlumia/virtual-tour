define(["jquery", "reveal", "services"], function ($, Reveal, Services) {
    Reveal.initialize({
        progress: false,
        history: true
    });

    Services.buildSideNav();

    Reveal.addEventListener('ready', function (event) {
        Services.setActive();
    });

    Reveal.addEventListener('slidechanged', function (event) {
        Services.setActive();
    });


});